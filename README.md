## Notes

Originally i used just one call to API you provided, but the response was huuuge with 
bunch of unnecessary data and it took more time to fetch than all pieces i really needed called separately (in async).
So i created DataProvider component that provides only that data that i eventually use.

to start the app run install dependencies with `yarn` and then `yarn start` 

DISCLOSURE:  
I was unable to retrieve assets from your invision app - there was just a message (how to upload them and what plugin to use for that)  
So i only used gradients, but putting some assets there is quite trivial task anyway.  

App and its folder structure is built with scaling in mind.

I tested it on Chrome, Safari and Firefox. I don't have MS Edge :/ so let's just hope ;D  


## Preview
![preview](./lottotland.gif)

#### Why React 16.8.0-alpha.1?

I saw it as a good opportunity to use edge practises :)  
React Hooks is definitely the future of react and soon will   
make many libraries obsolete (e.g. Recompose.js and one day maybe even Redux :o )

#### Why StyledComponents (SC) and not "classic" CSS?

I find SC more composable, flexible and overall more dev-friendly + it really has its place in React Community

#### Where is React Storybook?

At first I wanted to include Storybook, but since this is just small one-screen App it seemed bit like an overkill

### Why I excluded WorldMillions and FriLotto?

It seems like data structure for these 2 lotteries is inconsistent.  
In most cases "last.numbers" field is returning array of numbers [1, 20, 30...]  
But in case of those 2 lotteries, BE is returning [12341]  
Although it is the same type Array<number>, FE needs to parse this data further  
Obviously that's quite easy in data transformers, but that's just ugly.   
Long story short > In the age of Graph APIs even REST should return consistent data!
-- we usually call such APIs a Frontend harassment :D

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.  
Open [http://localhost:4000](http://localhost:4000) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.  

### `yarn run build`

Builds the app for production to the `build` folder.  
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.  
