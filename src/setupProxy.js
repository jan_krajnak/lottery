const proxy = require('http-proxy-middleware')

module.exports = function(app) {
  app.use(
    proxy('/api', {
      target: '' + process.env.API,
      changeOrigin: true,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      pathRewrite: { '^/api': '' },
      logLevel: 'debug',
      secure: false,
    })
  )
}
