import { Drawing, drawingTransformer } from './drawing'

export type Lottery = {
  name: string
  tagline: string
  kind: string
  accentColor: string
  last: Drawing
  next: Drawing
}

const nameMappings: { [key: string]: [string, string, string] } = {
  monWedOz: ['Mon & Wed Lotto', 'Monday & Wednesday Lotto', '#C43421'],
  keno247: ['Keno 24/7', 'Keno 24/7', '#F0C2D9'],
  polishKeno: ['Kenoland', 'Draws every 4 minutes', '#F0C2D9'],
  ozPowerBall: ['Powerball', 'Australian PowerBall', '#3957C8'],
  saturdayOz: ['Tattslotto', 'X lotto, Gold Lotto, Saturday Lotto', '#C43421'],
  ozLotto: ['Oz Lotto', 'Australian PowerBall', '#469E3D'],
  powerBall: ['US Power', 'US PowerBall', '#C43421'],
  worldMillions: ['World Millions', 'World Millions', '#131A27'],
  megaMillions: ['US Mega Millions', 'US MegaMillions', '#3856C6'],
  fridayLotto: ['Fri Lotto', 'Australian Friday Night Lotto', '#E0007B'],
}

export const lotteryTransformer = (key: string) => (data: any) => {
  const [name, tagline, accentColor] = nameMappings[key]
  return {
    kind: key,
    accentColor,
    name,
    tagline,
    last: drawingTransformer(data.last),
    next: drawingTransformer(data.next),
  }
}
