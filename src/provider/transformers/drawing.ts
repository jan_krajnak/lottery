import { pipe, values, pick, flatten } from 'ramda'

export type Drawing = {
  nr: number
  currency: string
  drawingDate: string
  numbers: number[]
  bonus: number[]
  jackpot: string
}

const parameters = ['nr', 'currency', 'drawingDate', 'numbers', 'jackpot']
const bonusParams = ['supplementary', 'bonus', 'powerballs']

export const drawingTransformer = (data: any) => {
  const common = pick(parameters, data)
  const bonus = pipe(
    pick(bonusParams),
    values,
    flatten
  )(data)

  return { ...common, bonus }
}
