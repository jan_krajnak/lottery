// transformer functions are perfect for mapping
// responses from Server to more manageable data structures
// this app is too simple so it doesn't need all returned data

export * from './drawing'
export * from './lottery'
export * from './keno247'
