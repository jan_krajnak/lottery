import { lotteryTransformer } from '../'

const DATA = {
  last: {
    nr: 1302,
    currency: 'EUR',
    date: {
      full: 'Die Lottozahlen vom Dienstag, den 29.01.2019',
      day: 29,
      month: 1,
      year: 2019,
      hour: 13,
      minute: 30,
      dayOfWeek: 'Dienstag',
    },
    closingDate: '29.01.2019, 13:00',
    lateClosingDate: '29.01.2019, 13:00',
    drawingDate: '29.01.2019, 13:30',
    numbers: [8, 9, 12, 20, 37, 39, 45],
    bonus: [36, 41],
    jackpot: '12',
    marketingJackpot: '12',
    specialMarketingJackpot: '12',
    climbedSince: 33,
    Winners: 0,
    odds: {
      rank0: { winners: 0, specialPrize: 0, prize: 0 },
      rank1: { winners: 0, specialPrize: 0, prize: 1190311367 },
      rank2: { winners: 0, specialPrize: 0, prize: 2192679 },
      rank3: { winners: 0, specialPrize: 0, prize: 187944 },
      rank4: { winners: 0, specialPrize: 0, prize: 18794 },
      rank5: { winners: 0, specialPrize: 0, prize: 2506 },
      rank6: { winners: 0, specialPrize: 0, prize: 1253 },
      rank7: { winners: 0, specialPrize: 0, prize: 626 },
    },
  },
  next: {
    nr: 1303,
    currency: 'EUR',
    date: {
      full: 'Die Lottozahlen vom Dienstag, den 05.02.2019',
      day: 5,
      month: 2,
      year: 2019,
      hour: 13,
      minute: 30,
      dayOfWeek: 'Dienstag',
    },
    closingDate: '05.02.2019, 13:00',
    lateClosingDate: '05.02.2019, 13:00',
    drawingDate: '05.02.2019, 13:30',
    jackpot: '15',
    marketingJackpot: '15',
    specialMarketingJackpot: '15',
    climbedSince: 34,
  },
}

test('Processed Lottery should have kind, name and tagline property', () => {
  expect(lotteryTransformer('ozLotto')(DATA)).toEqual({
    accentColor: '#469E3D',
    kind: 'ozLotto',
    last: {
      bonus: [36, 41],
      currency: 'EUR',
      drawingDate: '29.01.2019, 13:30',
      jackpot: '12',
      nr: 1302,
      numbers: [8, 9, 12, 20, 37, 39, 45],
    },
    name: 'Oz Lotto',
    next: {
      bonus: [],
      currency: 'EUR',
      drawingDate: '05.02.2019, 13:30',
      jackpot: '15',
      nr: 1303,
    },
    tagline: 'Australian PowerBall',
  })
})
