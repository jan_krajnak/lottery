import { drawingTransformer } from './drawing'

// so at the last moment i had to create this special-case transformer
// for keno247, because i encountered weird behaviour
// sometimes `last` returned numbers but sometimes NOT
// so i'm taking numbers from `past[0]`
// this API weirdness is infuriating to be honest
// + some API specification/documentation would be nice
export const keno247Transformer = () => (data: any) => {
  return {
    kind: 'keno247',
    accentColor: '#F0C2D9',
    tagline: 'Keno 24/7',
    name: 'Keno 24/7',
    last: drawingTransformer(data.past[0]),
    next: drawingTransformer(data.next),
  }
}
