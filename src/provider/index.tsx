import * as React from 'react'
import axios from 'axios'
import { useState, useEffect } from 'react'

export type DataProviderInputProps = {
  url: string
  children: any
  transformer?: (data: any) => any
}

export type DataProviderOutputProps<Data> = {
  data: Data
  error: boolean
  loading: boolean
}
export const DataProvider: React.FC<DataProviderInputProps> = ({
  url,
  transformer,
  children,
}) => {
  const [data, setData] = useState(null)
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(false)

  const fetchData = async () => {
    try {
      const { data } = await axios.get(url)
      const transformedData = transformer ? transformer(data) : data
      setData(transformedData)
    } catch (e) {
      setError(true)
    }
    setLoading(false)
  }

  useEffect(() => {
    fetchData()
  }, [])

  return children({ data, loading, error })
}
