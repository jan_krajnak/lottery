import * as React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { MainLayout, Header } from '../components'
import { MainScreen } from './main'

const content = (
  <Switch>
    <Route path={'/'} component={MainScreen} />
  </Switch>
)

export const App = () => (
  <BrowserRouter>
    <MainLayout
        header={<Header />}
        content={content} />
  </BrowserRouter>
)
