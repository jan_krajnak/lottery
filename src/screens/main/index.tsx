import * as React from 'react'
import { last } from 'ramda'
import { Flex, ArcTitle, Box, BoxProps } from '../../components'
import { breakpoints } from '../../theme'

// i moved all banners to their individual files
// it can be refactored further to have less components
// but let's leave trivial things for some other time :)
import { MonWedOz } from './parts/monWedOz'
import { Kenoland } from './parts/kenoland'
import { OzPowerBall } from './parts/ozPowerBall'
import { SaturdayOz } from './parts/saturdayOz'
import { OzLotto } from './parts/ozLotto'
import { Ad } from './parts/ad'
import {PowerBall} from "./parts/powerBall";
import {MegaMillions} from "./parts/megaMillions";
import {Keno247} from "./parts/keno247";

const lastBreakpoint = parseInt(last(breakpoints) || '1024')
export const bannerWidth = lastBreakpoint / 3
export const MainScreen: React.FC<BoxProps> = props => (
  <Box {...props}>
    <ArcTitle
      mt={20}
      alignItems={'center'}
      dark={'CHECK OUT'}
      light={'LATEST LOTTO RESULTS'}
    />
    <Flex flexDirection={'row'} flexWrap={'wrap'}>
      <MonWedOz />
      <Kenoland />
      <OzPowerBall />
      <SaturdayOz />
      <OzLotto />
    </Flex>
    <Ad />
    <ArcTitle alignItems={'center'} dark={'NEVER LOOSE A TICKET'} />
    <Flex flexDirection={'row'} flexWrap={'wrap'}>
        <PowerBall />
        <MegaMillions />
        <Keno247 />
    </Flex>
  </Box>
)
