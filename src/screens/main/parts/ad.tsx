import * as React from 'react'
import { Box, Flex, Text } from '../../../components/generic'

export const Ad: React.FC = () => (
  <Flex
    py={'bigPadding'}
    my={50}
    flex={1}
    flexDirection={['column', 'column', 'row']}
    alignItems={'center'}
    background={'#f5f5f5'}
    justifyContent={'center'}
  >
    <Box
      width={50}
      height={50}
      borderRadius={'50%'}
      background={'lightgray'}
      border={'1px dashed gray'}
      mx={20}
    />
    <Flex
      flexDirection={['column', 'column', 'row']}
      alignItems={'center'}
      justifyContent={'center'}
    >
      <Text fontWeight={500}>Over $350,000 in 6 days to 6 lucky winers!</Text>
      <Text>KENOLAND keeps that winning feeling rolling</Text>
      <Text pl={10} color={'themePrimary'} fontWeight={500}>
        Learn More
      </Text>
    </Flex>
  </Flex>
)
