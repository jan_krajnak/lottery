import * as React from 'react'
import { DataProvider, DataProviderOutputProps } from '../../../provider'
import { Lottery, lotteryTransformer } from '../../../provider/transformers'
import {
  Banner,
  BannerHeader,
  Bonus,
  Info,
  Links,
  Results,
} from '../../../components'
import { bannerWidth } from '..'

export const OzLotto: React.FC = () => (
  <DataProvider
    url={'api/drawings/ozLotto'}
    transformer={lotteryTransformer('ozLotto')}
  >
    {(output: DataProviderOutputProps<Lottery>) => {
      return (
        <Banner width={['100%', '100%', '50%', `${bannerWidth}px`]} {...output}>
          <BannerHeader
            {...output.data}
            background={'linear-gradient(#e53935, #e35d5b)'}
          >
            <Info {...output.data} />
          </BannerHeader>
          <Results mt={10} {...output.data} />
          <Bonus mt={10} {...output.data} />
          <Links />
        </Banner>
      )
    }}
  </DataProvider>
)
