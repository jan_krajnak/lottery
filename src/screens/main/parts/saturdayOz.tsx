import * as React from 'react'
import { DataProvider, DataProviderOutputProps } from '../../../provider'
import { Lottery, lotteryTransformer } from '../../../provider/transformers'
import {
  Banner,
  BannerHeader,
  Bonus,
  Info,
  Links,
  Results,
} from '../../../components'
import { bannerWidth } from '..'

export const SaturdayOz: React.FC = () => (
  <DataProvider
    url={'api/drawings/saturdayOz'}
    transformer={lotteryTransformer('saturdayOz')}
  >
    {(output: DataProviderOutputProps<Lottery>) => {
      return (
        <Banner width={['100%', '100%', '50%', `${bannerWidth}px`]} {...output}>
          <BannerHeader
            {...output.data}
            background={'linear-gradient(#00B4DB, #0083B0)'}
          >
            <Info {...output.data} />
          </BannerHeader>
          <Results mt={10} {...output.data} />
          <Bonus mt={10} {...output.data} />
          <Links />
        </Banner>
      )
    }}
  </DataProvider>
)
