import * as React from 'react'
import { DataProvider, DataProviderOutputProps } from '../../../provider'
import { Lottery, lotteryTransformer } from '../../../provider/transformers'
import {
  Banner,
  BannerHeader,
  Bonus,
  Info,
  Links,
  Results,
} from '../../../components'
import { bannerWidth } from '..'

export const OzPowerBall: React.FC = () => (
  <DataProvider
    url={'api/drawings/ozPowerBall'}
    transformer={lotteryTransformer('ozPowerBall')}
  >
    {(output: DataProviderOutputProps<Lottery>) => {
      return (
        <Banner width={['100%', '100%', '50%', `${bannerWidth}px`]} {...output}>
          <BannerHeader
            {...output.data}
            background={'linear-gradient(#D1913C, #FFD194)'}
          >
            <Info {...output.data} />
          </BannerHeader>
          <Results mt={10} {...output.data} />
          <Bonus mt={10} {...output.data} />
          <Links />
        </Banner>
      )
    }}
  </DataProvider>
)
