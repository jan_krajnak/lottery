import * as React from 'react'
import { DataProvider, DataProviderOutputProps } from '../../../provider'
import { Lottery, lotteryTransformer } from '../../../provider/transformers'
import {
  Flex,
  Text,
  Banner,
  BannerHeader,
  Countdown,
  Jackpot,
  Button,
} from '../../../components'
import { bannerWidth } from '..'

export const Kenoland: React.FC = () => (
  <DataProvider
    url={'api/drawings/polishKeno'}
    transformer={lotteryTransformer('polishKeno')}
  >
    {(output: DataProviderOutputProps<Lottery>) => {
      return (
        <Banner width={['100%', '100%', '50%', `${bannerWidth}px`]} {...output}>
          <BannerHeader
            {...output.data}
            background={'linear-gradient(#FC5C7D, #6A82FB)'}
          >
            <Countdown />
          </BannerHeader>
          <Flex flexDirection={'column'} alignItems={'center'}>
            <Jackpot py={20} value={'10'} />
            <Button
              primary
              borderRadius={3}
              width={180}
              height={60}
              flexDirection={'column'}
            >
              <Text color={'themeDark'} fontWeight={500} fontSize={12}>
                1 Quicky $1.00
              </Text>
              <Text color={'white'} fontWeight={800} mt={'5px'}>
                BET NOW
              </Text>
            </Button>
          </Flex>
        </Banner>
      )
    }}
  </DataProvider>
)
