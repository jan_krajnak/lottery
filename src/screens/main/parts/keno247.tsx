import * as React from 'react'
import { DataProvider, DataProviderOutputProps } from '../../../provider'
import {keno247Transformer, Lottery, lotteryTransformer} from '../../../provider/transformers'
import {
  Banner,
  BannerHeader,
  Bonus,
  Info,
  Links,
  Results,
} from '../../../components'
import { bannerWidth } from '..'

export const Keno247: React.FC = () => (
  <DataProvider
    url={'api/drawings/keno247'}
    transformer={keno247Transformer()}
  >
    {(output: DataProviderOutputProps<Lottery>) => {
      return (
        <Banner width={['100%', '100%', '50%', `${bannerWidth}px`]} {...output}>
          <BannerHeader
            {...output.data}
            background={'linear-gradient(#FC5C7D, #6A82FB)'}
          >
            <Info {...output.data} />
          </BannerHeader>
          <Results mt={10} {...output.data} />
          <Bonus mt={10} {...output.data} />
          <Links />
        </Banner>
      )
    }}
  </DataProvider>
)
