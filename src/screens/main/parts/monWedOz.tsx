import * as React from 'react'
import { DataProvider, DataProviderOutputProps } from '../../../provider'
import { Lottery, lotteryTransformer } from '../../../provider/transformers'
import {
  Banner,
  BannerHeader,
  Bonus,
  Info,
  Links,
  Results,
  Flex,
} from '../../../components'
import { bannerWidth } from '..'

export const MonWedOz: React.FC = () => (
  <DataProvider
    url={'api/drawings/monWedOz'}
    transformer={lotteryTransformer('monWedOz')}
  >
    {(output: DataProviderOutputProps<Lottery>) => {
      return (
        <Banner
          width={['100%', '100%', '100%', `${bannerWidth * 2}px`]}
          {...output}
        >
          <BannerHeader
            {...output.data}
            background={'linear-gradient(#e53935, #e35d5b)'}
          >
            <Info {...output.data} />
          </BannerHeader>
          <Flex>
            <Results mt={10} {...output.data} />
            <Bonus mt={10} {...output.data} />
          </Flex>
          <Links />
        </Banner>
      )
    }}
  </DataProvider>
)
