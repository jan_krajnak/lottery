import * as React from 'react'
import reset from 'styled-reset'
import { ThemeProvider, createGlobalStyle } from 'styled-components'

import {breakpointWidth, mainTheme} from '../../theme'
import { Flex, Screen } from '../generic'
import { Fonts } from '../../theme/fonts'
import styled from 'styled-components'
import { withBorder } from '../extensions'

const GlobalReset = createGlobalStyle`
  ${reset}
`
type MainLayoutProps = {
  content: React.ReactElement<any>
  header: React.ReactElement<any>
}

const FlexWithBorder = styled(Flex)`
  ${withBorder('bottom')}
`

export const MainLayout: React.FunctionComponent<MainLayoutProps> = ({
  content,
  header,
}) => (
  <React.Fragment>
    <GlobalReset />
    <Fonts />
    <ThemeProvider theme={mainTheme}>
      <Screen flex={1} flexDirection={'column'}>
        <FlexWithBorder justifyContent={'center'}>
          <Flex width={breakpointWidth}>{header}</Flex>
        </FlexWithBorder>
        <Flex flex={1} width={breakpointWidth} alignSelf={'center'}>
          {content}
        </Flex>
      </Screen>
    </ThemeProvider>
  </React.Fragment>
)
