import * as React from 'react'
import styled from 'styled-components'

import { Flex, FlexProps } from '../flex'
import { withFocus } from '../../extensions'
import { primary, VariationsProps } from './variations'

export type ButtonProps = FlexProps &
  VariationsProps & {
    title?: string
    disabled?: boolean
    onClick?(): void
    onKeyPress?(): void
  }

const ButtonWrapper: React.FC<ButtonProps> = styled(Flex).attrs({
  tabIndex: 0,
})`
  // extensions
  ${withFocus}
  
  // variations
  ${primary}
  
  // common
  justify-content: center;
  align-items: center;
`

export const Button: React.FC<ButtonProps> = props => {
  const { disabled, onClick, children } = props
  const handleClick = !disabled ? onClick : undefined
  const handleKeyPress = !disabled ? onClick : undefined

  return (
    <ButtonWrapper onClick={handleClick} onKeyPress={handleKeyPress} {...props}>
      {children}
    </ButtonWrapper>
  )
}
