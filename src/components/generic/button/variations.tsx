import * as React from 'react'
import { Theme } from '../../../theme'

// Each variation is applied only when
// appropriate Prop is used e.g. <Button primary />

export type VariationsProps = {
  primary?: boolean
}

export const primary = ({
  theme,
  primary,
}: { theme: Theme } & VariationsProps) =>
  primary
    ? ` background: linear-gradient(0.2turn, 
        ${theme.colors.gradientStart}, ${theme.colors.gradientStart}, ${
        theme.colors.gradientEnd
      });
        box-shadow: 0 0 20px 0 silver;
        
        :hover {
          background: ${theme.colors.gradientEnd};
        }
    `
    : ''
