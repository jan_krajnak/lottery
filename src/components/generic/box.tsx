// This is the main composable building block of UI
// Box utilises styled-system library which provides
// nice layouting experience including responsive behaviour
// all other components are based on Box

import * as React from 'react'
import styled from 'styled-components'
import {
  space,
  height,
  width,
  color,
  borderRadius,
  minWidth,
  minHeight,
  style,
  top,
  left,
  border,
  display,
  position,
  bottom,
  right,
  opacity,
  backgroundSize,
} from 'styled-system'

// some custom responsive style properties

const overflow = style({
  prop: 'overflow',
})

const background = style({
  prop: 'background',
  cssProperty: 'background',
  key: 'colors',
})

const cursor = style({
  prop: 'cursor',
  cssProperty: 'cursor',
})

export type BoxProps = {
  position?: 'relative' | 'absolute' | 'fixed'
  m?: string | number | string[]
  mx?: string | number | string[]
  my?: string | number | string[]
  ml?: string | number | string[]
  mr?: string | number | string[]
  mt?: string | number | string[]
  mb?: string | number | string[]
  p?: string | number | string[]
  px?: string | number | string[]
  py?: string | number | string[]
  pl?: string | number | string[]
  pr?: string | number | string[]
  pt?: string | number | string[]
  pb?: string | number | string[]
  display?: string | string[]
  left?: string | number | string[]
  bottom?: string | number | string[]
  right?: string | number | string[]
  top?: string | number | string[]
  height?: string | number | string[]
  width?: string | number | string[]
  minHeight?: string | number | string[]
  minWidth?: string | number | string[]
  color?: string | string[]
  borderRadius?: string | number
  border?: string | string[]
  opacity?: string | number
  cursor?: string | string[]
  background?: string | string[]
  backgroundSize?: string | string[]
}

export const Box: React.FC<BoxProps> = styled.div`
  box-sizing: border-box;
  position: relative;
  ${position}
  ${display}
  ${space}
  ${top}
  ${left}
  ${bottom}
  ${right}  
  ${height}
  ${minHeight}
  ${minWidth}
  ${width}
  ${color}
  ${borderRadius}
  ${border}
  ${opacity}
  ${backgroundSize}
  ${cursor}
  ${overflow}
  ${background}
`
