import * as React from 'react'
import styled from 'styled-components'
import {
  fontFamily,
  fontSize,
  fontWeight,
  textAlign,
  fontStyle,
} from 'styled-system'

import { Box, BoxProps } from './box'

export type TextProps = {
  fontFamily?: string
  fontSize?: string | number | string[] | number[]
  fontWeight?: string | number
  textAlign?: string
  fontStyle?: 'italic' | 'normal' | 'oblique' | 'unset'
}

export const Text: React.FC<TextProps & BoxProps> = styled(Box)`
  display: inline-block;
  font-family: 'DaxlinePro', sans-serif;
  ${fontFamily}
  ${fontSize}
  ${fontWeight}
  ${textAlign}
  ${fontStyle} 
`
