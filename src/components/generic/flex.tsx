import * as React from 'react'
import styled from 'styled-components'
import {
  flexWrap,
  flexDirection,
  flex,
  flexBasis,
  alignSelf,
  justifySelf,
  justifyContent,
  alignItems,
} from 'styled-system'

import { Box, BoxProps } from '.'

export type FlexProps = BoxProps & {
  flexWrap?: 'wrap' | 'nowrap' | 'wrap-reverse'
  flexDirection?: 'row' | 'column' | string[]
  flex?: string | number
  flexBasis?: string
  alignSelf?: string
  justifySelf?: string
  alignItems?: string
  justifyContent?: string
}

export const Flex: React.FC<FlexProps> = styled(Box)`
  display: flex;
  ${flexWrap}
  ${flexDirection}
  ${flex}
  ${flexBasis}
  ${alignSelf}
  ${alignItems}
  ${justifyContent}
  ${justifySelf}
`
