import * as React from 'react'
import styled from 'styled-components'
import { Flex, FlexProps } from '.'

export const Screen: React.FC<FlexProps> = styled(Flex)`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  overflow-x: hidden;
  overflow-y: auto;
`
