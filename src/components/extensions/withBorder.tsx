import * as React from 'react'
import { Theme } from '../../theme'

export const withBorder = (side: 'top' | 'bottom' | 'left' | 'right') => ({
  theme,
}: {
  theme: Theme
}) => `
    border-${side}: 1px solid ${theme.colors.border}
`
