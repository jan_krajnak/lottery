import * as React from 'react'
import { Theme } from '../../theme'

type WithFocusProps = {
  theme: Theme
  disabled?: boolean
}

export const withFocus = ({ theme, disabled }: WithFocusProps) => `
    user-select: none;
    :hover {
        cursor: ${disabled ? 'default' : 'pointer'};
    }
`
