import * as React from 'react'
import { Flex, FlexProps, Text } from '../../generic'
import { Number } from './number'
import { Lottery } from '../../../provider/transformers'

export const Results: React.FC<Lottery & FlexProps> = data => {
  const { last, ...rest } = data
  if (!last) return null
  const { numbers } = last
  return (
    numbers && (
      <Flex flexDirection={'column'} pl={'bigPadding'} {...rest}>
        <Text fontWeight={200}>Winning Numbers</Text>
        <Flex flexWrap={'wrap'}>
          {numbers.map((value, index) => (
            <Number key={`${value}-${index}`} value={value} />
          ))}
        </Flex>
      </Flex>
    )
  )
}
