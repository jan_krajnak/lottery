import * as React from 'react'
import {Flex, FlexProps, Text} from '../../generic'

export const Jackpot: React.FC<{ value: string } & FlexProps> = ({ value, ...rest }) => (
  <Flex flexDirection={'row'} alignItems={'flex-end'} {...rest}>
    <Text fontWeight={900}>
      <Text fontSize={25}>$</Text>
      <Text fontSize={40}>{value}</Text>
      <Text fontSize={25}>milion</Text>
    </Text>
  </Flex>
)
