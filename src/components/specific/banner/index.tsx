import * as React from 'react'
import { Box, BoxProps, Flex, FlexProps, Text } from '../../generic'
import { Lottery } from '../../../provider/transformers'
import loaderGif from '../../../assets/loader.gif'
import styled from 'styled-components'

type BannerProps = FlexProps & {
  data: Lottery
  loading?: boolean
  error?: boolean
  Slider?: React.FC<Lottery>
  accentColor?: string
}

const Loader = styled(Flex)`
  background: url(${loaderGif}) no-repeat;
  background-position-x: center;
  background-position-y: center;
  background-size: contain;
`

const BoxWithShadow: React.FC<BoxProps & { accentColor?: string }> = styled(
  Box
)`
  box-shadow: 0 0 12px -3px silver;
  overflow: hidden;
  padding-bottom: 50px;

  :after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 5px;
    height: 100%;
    background-color: ${({ accentColor }: any) => accentColor};
  }
`

export const Banner: React.FC<BannerProps> = props => {
  const { data, loading, error, children, ...rest } = props
  return (
    <React.Fragment>
      {data && (
        <Flex flexDirection={'column'} {...rest}>
          <BoxWithShadow
            minHeight={300}
            m={'smallPadding'}
            accentColor={data && data.accentColor}
          >
            {children}
          </BoxWithShadow>
        </Flex>
      )}
      {loading && (
        <Flex flexDirection={'column'} {...rest}>
          <BoxWithShadow minHeight={300} m={'smallPadding'}>
            <Loader height={300} {...rest} />
          </BoxWithShadow>
        </Flex>
      )}
      {error && (
        <Flex border={'1px solid lightgray'} {...rest}>
          <Text>Something went wrong :(</Text>
        </Flex>
      )}
    </React.Fragment>
  )
}
