import * as React from 'react'
import { Flex, Text } from '../../generic'
import styled from 'styled-components'
import { withBorder } from '../../extensions'

const Divider = styled(Flex)`
  ${withBorder('left')}
`
const BannerLink = styled(Text).attrs({
  fontWeight: 500,
  color: 'themePrimary',
})``

export const Links: React.FC = () => (
  <Flex
    position={'absolute'}
    bottom={10}
    right={0}
    p={'5px'}
    mt={'bigPadding'}
    pr={'bigPadding'}
    justifyContent={'flex-end'}
  >
    <BannerLink>Prize details</BannerLink>
    <Divider mx={'bigPadding'} />
    <BannerLink>Help & FAQ</BannerLink>
  </Flex>
)
