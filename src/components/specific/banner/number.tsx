import * as React from 'react'
import { Flex, Text } from '../../generic'

export const Number: React.FC<{ value: number; accentColor?: string }> = ({
  value,
  accentColor = 'lightgray',
}) => (
  <Flex
      width={35}
      height={35}
      mr={'5px'}
      mt={'5px'}
      borderRadius={'50%'}
      border={`1px solid ${accentColor}`}
      justifyContent={'center'}
      alignItems={'center'}
  >
    <Text>
      {value}
    </Text>
  </Flex>
)
