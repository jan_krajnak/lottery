import * as React from 'react'
import { Flex, FlexProps, Text } from '../../generic'
import { Number } from './number'
import { Lottery } from '../../../provider/transformers'

export const Bonus: React.FC<Lottery & FlexProps> = data => {
  const { last, ...rest } = data
  if (!last) return null
  const { bonus } = last
  const isBonus: boolean = bonus.length > 0
  return isBonus
    ? bonus && (
        <Flex flexDirection={'column'} pl={'bigPadding'} {...rest}>
          <Text fontWeight={200}>Supps</Text>
          <Flex flexWrap={'wrap'}>
            {bonus.map((value, index) => (
              <Number
                key={`${value}-${index}`}
                value={value}
                accentColor={data && data.accentColor}
              />
            ))}
          </Flex>
        </Flex>
      )
    : null
}
