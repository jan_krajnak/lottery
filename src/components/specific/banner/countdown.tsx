import * as React from 'react'
import { Flex, FlexProps, Text } from '../../generic'

const COUNTER = 240

export const Countdown: React.FC<FlexProps> = ({ ...rest }) => {
  const [time, changeTime] = React.useState(COUNTER)
  const minutes = Math.floor(time / 60)
  const seconds = time - minutes * 60
  const formattedSeconds = seconds < 10 ? `0${seconds}` : seconds

  const tick = () => {
    const val = time - 1
    if (val >= 0) {
      changeTime(time - 1)
    } else {
      changeTime(COUNTER)
    }
  }

  React.useEffect(() => {
    const timerID = setInterval(() => tick(), 1000)
    return function cleanup() {
      clearInterval(timerID)
    }
  })

  return (
    <Flex
      position={'absolute'}
      bottom={0}
      width={'100%'}
      flexDirection={'column'}
      justifyContent={'center'}
      pt={'smallPadding'}
      height={55}
      background={'rgba(0,0,0,0.3)'}
      {...rest}
    >
      <Text
        mb={'5px'}
        textAlign={'center'}
        color={'white'}
      >{`Next available draw: ${minutes}:${formattedSeconds}`}</Text>
    </Flex>
  )
}
