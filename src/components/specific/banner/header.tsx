import * as React from 'react'
import { Flex, FlexProps, Text } from '../../generic'
import { Lottery } from '../../../provider/transformers'

export const BannerHeader: React.FC<Lottery & FlexProps> = ({
  name,
  tagline,
  children,
  background,
}) => (
  <Flex height={150} flexDirection={'column'}>
    <Flex
      flex={1}
      flexDirection={'column'}
      p={'bigPadding'}
      background={background}
    >
      <Text fontWeight={800} fontSize={24} color={'white'}>
        {name}
      </Text>
      <Text fontSize={13} color={'white'}>
        {tagline}
      </Text>
    </Flex>
    {children}
  </Flex>
)
