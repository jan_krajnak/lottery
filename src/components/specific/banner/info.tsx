import * as React from 'react'
import { Flex, FlexProps, Text } from '../../generic'
import { Lottery } from '../../../provider/transformers'

export const Info: React.FC<Lottery & FlexProps> = ({ next, last, ...rest }) => {
  if (!last) return null
  const { nr, drawingDate } = last
  return (
    <Flex
      position={'absolute'}
      bottom={0}
      width={'100%'}
      flexDirection={'column'}
      justifyContent={'center'}
      pt={'smallPadding'}
      height={55}
      background={'rgba(0,0,0,0.3)'}
      {...rest}
    >
      <Text
        mb={'5px'}
        textAlign={'center'}
        fontWeight={900}
        color={'white'}
      >{`DRAW ${nr}`}</Text>
      <Text
        fontSize={14}
        textAlign={'center'}
        fontWeight={200}
        color={'white'}
      >
        {drawingDate}
      </Text>
    </Flex>
  )
}
