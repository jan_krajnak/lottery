import * as React from 'react'
import { Button, Flex, Text } from '../generic'
import { Logo } from './logo'

export const Header: React.FC = () => (
  <Flex
    flex={1}
    height={70}
    px={'bigPadding'}
    flexDirection={'row'}
    justifyContent={'space-between'}
    alignItems={'center'}
  >
    <Logo />
    <Flex>
      <Button
        primary
        borderRadius={3}
        width={90}
        height={40}
      >
        <Text color={'white'} fontWeight={800}>
          JOIN
        </Text>
      </Button>
    </Flex>
  </Flex>
)
