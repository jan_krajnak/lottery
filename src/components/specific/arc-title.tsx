import * as React from 'react'
import { Flex, FlexProps, Text } from '../generic'
import { ReactComponent as Arc } from '../../assets/arc.svg'

export const ArcTitle: React.FC<
  { dark?: string; light?: string } & FlexProps
> = ({ dark, light, ...rest }) => {
  const fs = [16, 20, 25, 30]
  return (
    <Flex flexDirection={'column'} justifyContent={'center'} {...rest}>
      <Flex flexDirection={'row'}>
        {dark && (
          <Text
            fontSize={fs}
            fontWeight={'900'}
            color={'themeDark'}
            pr={'smallPadding'}
          >
            {dark}
          </Text>
        )}
        {light && (
          <Text fontSize={fs} fontWeight={'900'} color={'themePrimary'}>
            {light}
          </Text>
        )}
      </Flex>
      <Arc width={100} />
    </Flex>
  )
}
