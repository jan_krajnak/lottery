import * as React from 'react'
import { ReactComponent as LogoSvg } from '../../assets/logo.svg'
import { Flex, Text } from '../generic'

export const Logo: React.FC = () => (
  <Flex
    flexDirection={'column'}
    justifyContent={'center'}
    alignItems={'center'}
  >
    <LogoSvg width={100} height={40} />
    <Text
      fontSize={12}
      fontStyle={'italic'}
      fontFamily={'Arial'}
      color={'themePrimary'}
    >
      Home of lotto betting
    </Text>
  </Flex>
)
