export const breakpoints = ['320px', '768px', '1024px']
export const breakpointWidth = ['100%', '100%', '100%', '1024px']

// based on the instructions
// im using 7px as main space unit
export const UNIT = 7

export type Theme = {
  colors: { [name: string]: string }
  space: { [name: string]: number }
  breakpoints: string[]
}

export const colors = {
  themePrimary: '#69A508',
  themeDark: '#012406',
  themeRed: '#C43421',
  themeBlue: '#3957C8',
  gradientStart: '#A5CD28',
  gradientEnd: '#69A508',
  border: '#D9DEDA',
}

export const space = {
  smallPadding: UNIT,
  bigPadding: UNIT * 2,
}

export const mainTheme: Theme = {
  breakpoints,
  colors,
  space,
}
