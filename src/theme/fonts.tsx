import { createGlobalStyle } from 'styled-components'

export const Fonts = createGlobalStyle`
  @font-face {
    font-family: "DaxlinePro";
    src:url("/fonts/ff-daxline-pro/200/normal.eot");
    src: url('/fonts/ff-daxline-pro/200/normal.eot?#iefix') format('embedded-opentype'), 
       url('/fonts/ff-daxline-pro/200/normal.woff2') format('woff2'),
       url('/fonts/ff-daxline-pro/200/normal.woff') format('woff'),
       url('/fonts/ff-daxline-pro/200/normal.ttf')  format('truetype');
    font-weight: 200;
    font-style: normal;
}
  @font-face {
    font-family: "DaxlinePro";
    src:url("/fonts/ff-daxline-pro/400/normal.eot");
    src: url('/fonts/ff-daxline-pro/400/normal.eot?#iefix') format('embedded-opentype'), 
       url('/fonts/ff-daxline-pro/400/normal.woff2') format('woff2'),
       url('/fonts/ff-daxline-pro/400/normal.woff') format('woff'),
       url('/fonts/ff-daxline-pro/400/normal.ttf')  format('truetype');
    font-weight: 400;
    font-style: normal;
}
  @font-face {
    font-family: "DaxlinePro";
    src:url("/fonts/ff-daxline-pro/500/normal.eot");
    src: url('/fonts/ff-daxline-pro/500/normal.eot?#iefix') format('embedded-opentype'), 
       url('/fonts/ff-daxline-pro/500/normal.woff2') format('woff2'),
       url('/fonts/ff-daxline-pro/500/normal.woff') format('woff'),
       url('/fonts/ff-daxline-pro/500/normal.ttf')  format('truetype');
    font-weight: 500;
    font-style: normal;
}
  @font-face {
    font-family: "DaxlinePro";
    src:url("/fonts/ff-daxline-pro/700/normal.eot");
    src: url('/fonts/ff-daxline-pro/700/normal.eot?#iefix') format('embedded-opentype'), 
       url('/fonts/ff-daxline-pro/700/normal.woff2') format('woff2'),
       url('/fonts/ff-daxline-pro/700/normal.woff') format('woff'),
       url('/fonts/ff-daxline-pro/700/normal.ttf')  format('truetype');
    font-weight: 700;
    font-style: normal;
}
  @font-face {
    font-family: "DaxlinePro";
    src:url("/fonts/ff-daxline-pro/800/normal.eot");
    src: url('/fonts/ff-daxline-pro/800/normal.eot?#iefix') format('embedded-opentype'), 
       url('/fonts/ff-daxline-pro/800/normal.woff2') format('woff2'),
       url('/fonts/ff-daxline-pro/800/normal.woff') format('woff'),
       url('/fonts/ff-daxline-pro/800/normal.ttf')  format('truetype');
    font-weight: 800;
    font-style: normal;
}
  @font-face {
    font-family: "DaxlinePro";
    src:url("/fonts/ff-daxline-pro/900/normal.eot");
    src: url('/fonts/ff-daxline-pro/900/normal.eot?#iefix') format('embedded-opentype'), 
       url('/fonts/ff-daxline-pro/900/normal.woff2') format('woff2'),
       url('/fonts/ff-daxline-pro/900/normal.woff') format('woff'),
       url('/fonts/ff-daxline-pro/900/normal.ttf')  format('truetype');
    font-weight: 900;
    font-style: normal;
}
`
